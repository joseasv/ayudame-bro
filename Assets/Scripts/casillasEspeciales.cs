﻿using UnityEngine;
using System.Collections;

public class casillasEspeciales : MonoBehaviour {

	public bool activado;
	public GameObject[] objeto;
	public string[] objetoNombre;

	private int opcion;
	private bool placed;

	void Start(){
		activado = false;
		placed = false;
		opcion = Random.Range (0, 2);

	}

	// Update is called once per frame
	void FixedUpdate () {
		if (placed == true) {
			objeto[opcion].transform.position = new Vector3 (transform.position.x,transform.position.y,-2);
		}
	}

	void OnTriggerEnter2D(Collider2D otro){
		if (otro.name == objetoNombre[opcion]) {
			otro.transform.position = new Vector3(transform.position.x, transform.position.y, -2);
			placed = true;
			activado = true;
			//renderer.enabled = false;
		}
	}

	public bool estaActivado(){
		return activado;
	}
}
